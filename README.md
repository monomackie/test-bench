# Test Bench

This repo is for integration (or functionnal) testing on the **device** side of the Monomackie project.

In order to do that, we isolate the project and control its inputs (serial port and buttons/slide), and check if we get the ouputs we expect (serial port).

## Use it

For now, please refer to the team's documentation about how to compile, upload on the tester (Raspberry Pi Zero W) and run the tests.

